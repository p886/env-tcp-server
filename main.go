package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"regexp"
	"time"
)

var timeoutSeconds int = 10
var listenPort int = 1234
var graphitePort int = 2003
var payloadRegex = regexp.MustCompile("indoor\\.(humidity|temperature) \\d{2}\\.\\d{2}")

func main() {
	log.Println("Server started")
	// Listen on TCP port 8080 on all available unicast and
	// anycast IP addresses of the local system.
	l, err := net.Listen("tcp", fmt.Sprintf(":%d", listenPort))
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
	for {
		// Wait for a connection.
		connection, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		// Handle the connection in a new goroutine.
		// The loop then returns to accepting, so that
		// multiple connections may be served concurrently.
		go handleConnection(connection)
	}
}

func handleConnection(connection net.Conn) {
	deadline := time.Now().Add(time.Duration(timeoutSeconds) * time.Second)
	err := connection.SetDeadline(deadline)
	if err != nil {
		log.Printf("connection timed out: %s\n", err.Error())
		return
	}
	payload, err := bufio.NewReader(connection).ReadString('\n')
	if err != nil {
		log.Printf("error reading payload: %s\n", err.Error())
		return
	}
	log.Printf("reading from conn: %s\n", payload)
	err = broadcastToGraphite(payload)
	if err != nil {
		log.Printf("Error broadcasting to Grahpite: %s", err)
		return
	}
	// Shut down the connection.
	err = connection.Close()
	if err != nil {
		log.Println("error closing connection: %#v", err)
	}
}

func broadcastToGraphite(payload string) error {
	relevantParts := payloadRegex.FindString(payload)

	if relevantParts != "" {
		unixTimestamp := time.Now().Unix()
		message := fmt.Sprintf("%s %d\n", relevantParts, unixTimestamp)
		connection, err := net.Dial("tcp", fmt.Sprintf("localhost:%d", graphitePort))
		if err != nil {
			return err
		}
		_, err = connection.Write([]byte(message))
		if err != nil {
			return err
		}
		log.Printf("Broadcasted to Grahpite: %s", message)
		err = connection.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
